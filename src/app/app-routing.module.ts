import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InvoicesDashboardComponent } from './invoices/invoices-dashboard/invoices-dashboard.component';
import { UsersDashboardComponent } from './users/users-dashboard/users-dashboard.component';

const routes: Routes = [
  {path: 'users', component: UsersDashboardComponent,
    loadChildren: () => import('./users/users.module').then(m=>m.UsersModule)},
  {path: 'invoices', component: InvoicesDashboardComponent,
    loadChildren: () => import('./invoices/invoices.module').then(m=>m.InvoicesModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
