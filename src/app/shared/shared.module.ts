import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './layout/header/header.component';
import { NavbarComponent } from './layout/navbar/navbar.component';
import { FooterComponent } from './layout/footer/footer.component';
import { JumbotronComponent } from './view/jumbotron/jumbotron.component';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatFormFieldModule} from '@angular/material/form-field';

@NgModule({
  declarations: [
    HeaderComponent,
    NavbarComponent,
    FooterComponent,
    JumbotronComponent
  ],
  imports: [
    CommonModule,
    MatDatepickerModule,
    MatFormFieldModule
  ],
  exports: [
    CommonModule,
    HeaderComponent,
    NavbarComponent,
    FooterComponent,
    JumbotronComponent,
    MatDatepickerModule,
    MatFormFieldModule
  ]
})
export class SharedModule { }
