import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvoicesRoutingModule } from './invoices-routing.module';
import { InvoicesDashboardComponent } from './invoices-dashboard/invoices-dashboard.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    InvoicesDashboardComponent
  ],
  imports: [
    InvoicesRoutingModule,
    SharedModule
  ]
})
export class InvoicesModule { }
