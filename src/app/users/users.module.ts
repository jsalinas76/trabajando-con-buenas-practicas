import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersDashboardComponent } from './users-dashboard/users-dashboard.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    UsersDashboardComponent
  ],
  imports: [
    UsersRoutingModule,
    SharedModule
  ]
})
export class UsersModule { }
